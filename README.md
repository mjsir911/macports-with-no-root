# MacPorts Installation Without Root

## Xcode CLI Tools
The first thing that has to be done is for the macbook to have the Xcode CLI Tools. In order to do so, run the command below and press accept for all of the prompts. If you are on battery power, there will be another prompt asking to continue on battery power. The Xcode installation takes about 15 minutes so you want to be sure to have enough battery to go through the entire procedure.
```bash
xcode-select --install
```

This is how you can install the MacPorts Package Manager without root privileges for Mr. Elkner's class :D.
To run the script, run the command below:
```bash
bash <(curl -s https://gitlab.com/risa_boi/macports-with-no-root/raw/master/macports_no_root.sh)
```

# SIDENOTES

If the `port` command after installation is not found, run the following.
```bash
source ~/.bash_profile
```

# Explanation of Script

This part of the script creates two directories in your `$HOME` folder (aka. `~`). One of them is for the macports files to go, and the other is for macports to install to.
```bash
mkdir $HOME/macports-installer $HOME/.macports
```

This part of the script will download the installer files to `$HOME/macports-installer`.
```bash
curl -o $HOME/macports-installer/MacPorts-2.5.3.tar.gz https://distfiles.macports.org/MacPorts/MacPorts-2.5.3.tar.gz
```

The `tar` part of the installer untars (aka. unzips) the macports files into the `$HOME/macports-installer` file.
```bash
tar xf $HOME/macports-installer/MacPorts-2.5.3.tar.gz -C $HOME/macports-installer
```

After the files are extracted, this part changes your directory to `$HOME/macports-installer/MacPorts-2.5.3/` and configures it to your system.
```bash
cd $HOME/macports-installer/MacPorts-2.5.3/
./configure --enable-readline \
	  --with-no-root-privileges \
	  --prefix=$HOME/.macports
```

The macports installer, after it is configured, needs to be compiled. There will be a lot of text, and none of it matters unless there is an error.
```bash
make -j3
make install
```

Now onto the `$PATH` variable. This variable tells the computer where to look for commands. I append the `$HOME/.macports/bin` directory to the `$PATH` variable, and OVERWRITES the `~/.bash_profile` file to add it. I used the `~` Tilde key because `$HOME` didn't work.
```bash
export PATH=$PATH:$HOME/.macports/bin # Adds to PATH
echo "export PATH=$PATH" > ~/.bash_profile # Exports to bash_profile so it appends to PATH on startup
source ~/.bash_profile # Sources it so that you can use it right away :)
```

Update the macports system and delete the installer files that aren't required.
```bash
$HOME/.macports/bin/port -v selfupdate		# Update
rm -rf $HOME/macports-installer
```

## Installing packages in MacPorts

Using the command `port install [package]` you can install whatever package you need.
In order to search for specific packages, please visit the MacPorts repository located here: https://macports.org/ports.php
(I am not the creator nor am I a developer of MacPorts).

#!/bin/bash
MACPORTS=$HOME/.local/macports
VERSION=2.5.4
MACPORTS_INSTALLER=$HOME/macports-installer

if [ -d $MACPORTS ]; then
	  rm -rf $MACPORTS
	  rm -rf $MACPORTS_INSTALLER
	  rm -rf $HOME/.macports.bash
	  sed -i.backup '/.macports.bash/d' $HOME/.bashrc
	  sed -i.backup '/.macports.bash/d' $HOME/.bash_profile
fi

# Create a MacPorts installer folder in your home directory (~), as well as the MacPorts installation directory
mkdir $MACPORTS_INSTALLER $MACPORTS

# Download the MacPorts installer
curl -o $HOME/macports-installer/MacPorts-$VERSION.tar.gz https://distfiles.macports.org/MacPorts/MacPorts-$VERSION.tar.gz

# Untar (unzip) the files 
tar xvf $HOME/macports-installer/MacPorts-$VERSION.tar.gz -C $HOME/macports-installer

# Change to directory and configure Macports to not use root and set prefix to $HOME/.macports
cd $MACPORTS_INSTALLER/MacPorts-$VERSION/
./configure --enable-readline \
	  --with-no-root-privileges \
	  --prefix=$MACPORTS

# Compile the system and then install to $HOME/.macports
make -j3
make install

# Because we did not write the file to a root-protected file, the computer doesn't know that we installed it.
# In order to get around that, you tell the computer to look in the $HOME/.macports/bin directory for commands
echo "source $HOME/.macports.bash" >> $HOME/.bashrc	# Adds to PATH
echo "source $HOME/.macports.bash" >> $HOME/.bash_profile # I don't know which one to use so I'm using both of them;
echo "export PATH=$PATH:$MACPORTS/bin" > $HOME/.macports.bash
# Fix DISPLAY and start XQuartz on startup
echo "echo \"Starting quartz-wm: may take some time. If you do not want to do tkinter work, press Control-C to exit.\"" >> $HOME/.macports.bash
echo "$MACPORTS/bin/quartz-wm" >> $HOME/.macports.bash
echo "export DISPLAY=/tmp/.X11-unix/X0" >> $HOME/.macports.bash

# Sources ~/.macports.bash in order to use ports quickly
. $HOME/.macports.bash


# Update MacPorts
$MACPORTS/bin/port -v selfupdate		# Update
cd $HOME
rm -rf $MACPORTS_INSTALLER

# Install xorg-server, Tkinter, and python37, as well as fixing XQuartz to work w/o root
yes | $MACPORTS/bin/port install python27
$MACPORTS/bin/port -f activate python27
yes | $MACPORTS/bin/port install python37
$MACPORTS/bin/port -f activate python37
sed -i.backup '/port:xinit/d' $MACPORTS/var/macports/sources/rsync.macports.org/macports/release/tarballs/ports/x11/xorg-server/Portfile
yes | $MACPORTS/bin/port install xorg-server py37-tkinter

echo "DONE\a"
echo "Please restart your temrinal."
exit 0
